#include <iostream>
#include <vector>
#include <algorithm>

int main() {
    std::vector<int> v(5);
    std::cin >> v[0] >> v[1] >> v[2] >> v[3] >> v[4];
    std::sort(v.begin(), v.end(), std::greater<int>());
    std::cout << v[0] << " " 
              << v[1] << " "
              << v[2] << " "
              << v[3] << " "
              << v[4] << std::endl;
}
