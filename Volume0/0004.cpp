#include <iostream>
#include <iomanip>

int main() {
    double a, b, c, d, e, f;
    double EPS = 1e-5;
    while(std::cin >> a >> b >> c >> d >> e >> f) {
        double det = a*e - b*d;
        double x = (c*e - b*f) / det;
        double y = (a*f - c*d) / det;
        // -0.000を0.000に修正しないと通らない
        if(-EPS < x && x < EPS) x = 0;
        if(-EPS < y && y < EPS) y = 0;
        std::cout << std::fixed << std::setprecision(3) << x << " " << y << std::endl;
    }
    return 0;
}
