#include <iostream>

int main() {
    int heights[3] = {-1, -1, -1};
    int n;
    for(int i=0; i<10; i++) {
        // 入力と並行して上位3つだけを保存しておく
        std::cin >> n;
        if(n >= heights[0]) {
            heights[2] = heights[1];
            heights[1] = heights[0];
            heights[0] = n;
        }
        else if(n >= heights[1]) {
            heights[2] = heights[1];
            heights[1] = n;
        }
        else if(n >= heights[2]) {
            heights[2] = n;
        }
    }
    for(int i=0; i<3; i++) std::cout << heights[i] << std::endl;
    return 0;
}
