#include <iostream>
#include <math.h>

int main() {
    int n;
    double debt = 100000;
    std::cin >> n;
    for(int i=1; i<=n; i++) {
        debt = debt * 1.05;
        // 1000円以下を切り上げる
        debt = 1000 * ceil(debt/1000);
    }
    std::cout << (int)debt << std::endl;
    return 0;
}
