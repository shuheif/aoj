#include <iostream>

int gcd(int a, int b) {
    if(b == 0) return a;
    return gcd(b, a%b);
}

// a * bの値が非常に大きくなり得るので, intでは桁が足りないことがある.
// ただし, 除算を先に行えばintだけでもいける.
int lcm(int a, int b) {
    return a * (b / gcd(a, b));
}

int main() {
    int a, b;
    while(std::cin >> a >> b) {
        std::cout << gcd(a, b) << " " << lcm(a, b) << std::endl;
    }
}
