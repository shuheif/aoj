#include <iostream>
#include <algorithm>

bool is_right(int a, int b, int c) {
    int sides[3] = {a, b, c};
    std::sort(sides, sides+3);
    return sides[2]*sides[2] == sides[1]*sides[1] + sides[0]*sides[0];
}

int main() {
    int itr, a, b, c;
    std::cin >> itr;
    while(itr--) {
        std::cin >> a >> b >> c;
        if(is_right(a, b, c)) std::cout << "YES" << std::endl;
        else std::cout << "NO" << std::endl;
    }
    return 0;
}
