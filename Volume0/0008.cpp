#include <iostream>

// もう少し綺麗に書けないものか
int comb(int n, int m) {
    int ans;
    if(n < 0 || m*9 < n) ans = 0;
    else if(n == 0 || m*9 == n) ans = 1;
    else {
        ans = 0;
        for(int i=0; i<=9; i++) ans += comb(n-i, m-1);
    }
    return ans;
}

int main() {
    int n;
    while(std::cin >> n) std::cout << comb(n, 4) << std::endl;
    return 0;
}
