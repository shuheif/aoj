// ToDo: コードが汚すぎるので修正する

#include <iostream>

// 0008: Sum of 4 Integers をメモ化した
#define SIZE_MEMO 4000
int memo[4][SIZE_MEMO];

int comb(int n, int m) {
    int ans;
    if(n < 0 || m*1000 < n) ans = 0;
    else if(n == 0 || m*1000 == n) ans = 1;
    else {
        if(n < SIZE_MEMO) {
            if(memo[m-1][n-1] > 0) {
                return memo[m-1][n-1];
            }
        }
        ans = 0;
        for(int i=0; i<=1000; i++) ans += comb(n-i, m-1);
    }
    //std::cout << m << n << std::endl;
    if(n > 0 && m > 0) memo[m-1][n-1] = ans;
    return ans;
}

int main() {
    int n;
    while(std::cin >> n) std::cout << comb(n, 4) << std::endl;
    return 0;
}
