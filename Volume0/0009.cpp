#include <iostream>
#include <vector>


std::vector<int> seive(int n) {
    std::vector<int> is_prime(n+1);
    for(int i=0; i<=n; i++) is_prime[i] = 1;
    is_prime[0] = 0;
    is_prime[1] = 0;
    for(int i=2; i<=n; i++) {
        if(is_prime[i]==1) {
            for(int j=i*2; j<=n; j+=i) {
                is_prime[j] = 0;
            }
        }
    }
    return is_prime;
}

int main() {
    int n;
    while(std::cin >> n) {
        int cnt = 0;
        std::vector<int> is_prime = seive(n);
        for(int i=0; i<is_prime.size(); i++) {
            if(is_prime[i]) {
                cnt++;
            }
        }
        std::cout << cnt << std::endl;
    }
    return 0;
}
